// Надіслати AJAX запит

function getFilms() {
  fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(films => listFilms(films))
    .catch(error => console.error(error));
  }
  
  // Отримати список фільмів

  function listFilms(films) {
    const filmsContainer = document.getElementById('films');
    films.forEach(function (film) {
      const filmElements = document.createElement('div');
      filmElements.innerHTML = '<h2>Episode ' + film.episodeId + ': ' + film.name + '</h2><p>' + film.openingCrawl + '</p><div id="characters' + film.episodeId + '"></div>';
      filmsContainer.appendChild(filmElements);
      getCharacters(film.episodeId, film.characters);
    });
  }
  
  // отримати з сервера список персонажів

  function getCharacters(episodeId, characterUrls){
    const charactersContainer = document.getElementById('characters' + episodeId);
    charactersContainer.innerHTML = '<div class="loader"></div>';
  
    Promise.all(characterUrls.map(url => fetch(url)
    .then(response => response.json())))
    .then(characters => {
      charactersContainer.innerHTML = '';
      characters.forEach(function(character){
        const characterElement = document.createElement('div');
        characterElement.textContent = character.name;
        charactersContainer.appendChild(characterElement);
      })
    })
    .catch(error => console.error(error))
  }
  
  getFilms();
